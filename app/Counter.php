<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Counter extends Model
{
    //
    protected $table = 'counter';

    protected $fillable = [
        'user_id','berkas_masuk','berkas_keluar','progress_berkas','berkas_selesai'
    ];

    public $timestamps = false;
}
