<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class UserUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        
        return [
            //
            'name' => ['required','string','max:255',
            Rule::unique('users', 'name')->ignore(Auth::user()->id)
        ],
            'NIM' => ['required','string','max:50'],
            'jenis_kelamin' => ['required','string','max:10'],
            'nomor_hp' => ['required','string','max:50'],
            'TTL' => ['required','string','max:255'],
        ];
    }
}
