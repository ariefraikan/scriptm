<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;






use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

Use App\fileupload;
Use App\User;

class ScriptmController extends Controller
{
    //pindah page

    public function beranda(){
       return view('Beranda');
    }

    public function galeri(){
        $fileupload = DB::table('fileupload')->where('user_id',Auth::user()->id)->paginate(10);
        return view('Galeri',['fileupload' => $fileupload]);
    }

    public function kontak(){
        return view('Kontak');
    }

    public function profil(){
       return view('profil');
    }

    public function edit(){
        $user = Auth::user();
        return view('users.edit', compact('user'));
    }

    public function editkeamanan(){
        $user = Auth::user();
        return view('users.editkeamanan', compact('user'));
    }

    public function pengajuanskma(){
        $file = fileupload::get();
		return view('pengajuanSKMA',['file' => $file]);
    }

    public function login(){
        return view('login');
    }

    public function Berkaskeluar(){
        $fileupload = DB::table('fileupload')->where('user_id',Auth::user()->id)->paginate(10);
        return view('BerkasKeluar',['fileupload' => $fileupload]);
    }

    public function Berkasmasuk(){
        $fileupload = DB::table('fileupload')->where('user_id',Auth::user()->id)->paginate(10);
        return view('BerkasMasuk',['fileupload' => $fileupload]);
    }

    public function Progressberkas(){
        $fileupload = DB::table('fileupload')->where('user_id',Auth::user()->id)->paginate(10);
        return view('ProgressBerkas',['fileupload' => $fileupload]);
    }

    public function Berkasselesai(){
        $fileupload = DB::table('fileupload')->where('user_id',Auth::user()->id)->paginate(10);
        return view('BerkasSelesai',['fileupload' => $fileupload]);
    }

   
    //upload file SKMA(gambar)
    public function uploadSKMA(Request $request){
        
		$this->validate($request, [
			'file' => 'required|file|image|mimes:jpeg,png,jpg|max:10000',
            'file2' => 'required|file|image|mimes:jpeg,png,jpg|max:10000',
            'user_id' => 'required',
            'pesan',
		]);
 
		// menyimpan data file yang diupload ke variabel $file
        $file = $request->file('file');
        $pesan = ('pesan');
        
        $nama_file = $file->getClientOriginalName();
        
 
      	        // isi dengan nama folder tempat kemana file diupload
		$tujuan_upload = 'data_file';
       
        $file->move($tujuan_upload,$nama_file);
       
 
		fileupload::create([
			'file' => $nama_file,
            'user_id' =>Auth::user()->id,
            'pesan' =>$pesan,
        ]);
       
        

        // menyimpan data file yang diupload ke variabel $file
        $file = $request->file('file2');
        
 
        $nama_file = $file->getClientOriginalName();
        
 
      	        // isi dengan nama folder tempat kemana file diupload
		$tujuan_upload = 'data_file';
        $file->move($tujuan_upload,$nama_file);
       
 
		fileupload::create([
			'file' => $nama_file,
            'user_id' =>Auth::user()->id,
            'pesan' =>$pesan,
        ]);
        
        return redirect('/home');
    }
    
    
    
}

