<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Counter;
use Auth;
use Hash;
use Illuminate\Support\Facades\DB;

class AuthController extends Controller
{
    public function register()
    {

      return view('register');
    }

    public function storeUser(Request $request)
    {
       

        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|max:255|unique:users',
            'password' => 'required|string|min:8|confirmed',
            'password_confirmation' => 'required',
            'NIM',
            'jenis_kelamin',
            'nomor_hp',
            'TTL',
            'email_not_attach',
            'role_akun' => 'required',
        ],
        [
          'name.required' => 'Harap nama diisi',
        ]);
        $blank = ('');
        $blank2 = ('0');
        
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'NIM' => $blank,
            'jenis_kelamin' => $blank,
            'nomor_hp' => $blank,
            'TTL' => $blank,
            'email_not_attach' => $blank,
            'role_akun' => $request->role_akun,
        ]);

        counter::create([
          'user_id' => $user->id,
          'berkas_masuk' => $blank2,
          'berkas_keluar' => $blank2,
          'progress_berkas' => $blank2,
          'berkas_selesai' => $blank2,
        ]);

        return redirect('/');
    }

    public function login()
    {

      return view('login');
    }

    public function authenticate(Request $request)
    {
      
        $request->validate([
            'email' => 'required|string',
            'password' => 'required|string',
        ]);

        $credentials = $request->only('email', 'password',);

        if (Auth::attempt($credentials)) {
            return redirect()->intended('home');
          
        }

        return redirect('login')->with('error', 'Oppes! You have entered invalid credentials');
    }

    public function logout() {
      Auth::logout();

      return redirect('login');
    }

    public function home()
    {
      $counter = DB::table('counter')->where('user_id',Auth::user()->id)->get();

      if(Auth::user()->role_akun == 'Mahasiswa'){
        return view('Beranda',['counter' => $counter]);
        }
      if(Auth::user()->role_akun == 'Staff'){
        return view('Beranda_Staff',['counter' => $counter]);
      }
    }
}