<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UserUpdate;
use App\User;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Hash;


class ProfileController extends Controller
{
    //
   
    public function edit(User $user)
    {
        
        $user = Auth::user();
        return view('users.edit', compact('user'));
    }

    public function security(User $user)
    {
        
        $user = Auth::user();
        return view('users.security', compact('user'));
    }

    public function update(User $user)
    {
       
        $this->validate(request(), [
            'name' => 'required|string|max:225',
            'NIM' => 'required|string|max:50',
            'jenis_kelamin' => 'required|string|max:10',
            'nomor_hp' => 'required|string|max:50',
            'TTL' => 'required|string|max:255',
            
        ]);

        $user->name = request('name');
        $user->NIM = request('NIM');
        $user->jenis_kelamin = request('jenis_kelamin');
        $user->nomor_hp = request('nomor_hp');
        $user->TTL = request('TTL');
        $user->save();
        return back();
    }

    public function updatesecurity(User $user,Request $request)
    {
       $this->validate(request(), [
            'email_not_attach' => 'required|string|max:255',
            'email' => 'required|string',
            'password_lama' =>'required',
            'password' => 'required|string|min:8',
            'konfirmasi' => 'required|same:password',
            
            
        ]);
        
        $data = $request->all();
        $user = User::find(auth()->user()->id);
        $password = Hash::check($data['password_lama'],$user->password);
        if(!Hash::check($data['password_lama'], $user->password)){
            return($password);
        }else{
        $user->email_not_attach = request('email_not_attach');
        $user->email = request('email');
        $user->password = bcrypt(request('password'));
        $user->save();
        return back();
        }
    }
}
