<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/







//login
Route::get('/','Auth\AuthController@login')->name('login');
Route::get('/login', 'Auth\AuthController@login')->name('login');
Route::post('/login', 'Auth\AuthController@authenticate');
Route::get('logout', 'Auth\AuthController@logout')->name('logout');

Route::get('/home', 'Auth\AuthController@home')->name('home');


Route::get('/register', 'Auth\AuthController@register')->name('register');
Route::post('/register', 'Auth\AuthController@storeUser');

//sesudah login
Route::group(['middleware' => 'auth'], function () {

    //apabila tidak login
    Route::get('/login', 'Auth\AuthController@login');
    
    //Pindah page halaman
    Route::get('/home', 'Auth\AuthController@home')->name('home');
    Route::get('/Galeri', 'ScriptmController@galeri');
    Route::get('/Kontak', 'ScriptmController@kontak');
    Route::get('/Profil', 'ScriptmController@profil');
    Route::get('/edit', 'ScriptmController@edit');
    Route::get('/editkeamanan', 'ScriptmController@editkeamanan');
    Route::get('/PengajuanSKMA', 'ScriptmController@pengajuanskma');
    Route::get('/Berkaskeluar', 'ScriptmController@Berkaskeluar');
    Route::get('/Berkasmasuk', 'ScriptmController@Berkasmasuk');
    Route::get('/Progressberkas', 'ScriptmController@Progressberkas');
    Route::get('/Berkasselesai', 'ScriptmController@Berkasselesai');

    //edit profil
    Route::get('users/{user}',  ['as' => 'users.edit', 'uses' => 'ProfileController@edit']);
    Route::patch('users/{user}/update',  ['as' => 'users.update', 'uses' => 'ProfileController@update']);
    Route::patch('users/{user}/updatesecurity',  ['as' => 'users.updatesecurity', 'uses' => 'ProfileController@updatesecurity']);


    //upload SKMA
    Route::post('/upload_SKMA','ScriptmController@uploadSKMA');
    //upload 
    
});

