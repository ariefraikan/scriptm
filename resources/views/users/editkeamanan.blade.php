@extends('master')

@section('konten')
<div class=container-fluid>
        <div class=row>
            <div class=col-lg-12>
                <br/>
                <br/>
                <br/>
            </div>
        </div>
        <div class=row>
            <div class=col-lg-2>
                <div class=sidebar>
                    <div id=profile_picture>
                        <img src="Profile_Picture.png"/>
                    </div>
                    <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
                    <ul>
                        <li><a style="color:#FFFFFF;" href="/Profil">Profil Saya</a></li>
                        <li><a style="color:#FFFFFF;" href="/edit">Ubah Profil</a></li>
                        <li class=sidebar_menu><a style="color:#FFFFFF;" href="/editkeamanan">Ubah Keamanan</a></li>
                    </ul>
					<div class=sidebar_exit>
						<h1><a href="{{ route('logout') }}">Keluar</a></h1>
                    </div>
                </div>
            </div>
            <div class=col-lg-10>
                <br/><br/><br/>
                <div class=kotak_ubahprofil>
                    <h1>Keamanan</h1>
                    <hr style="border: 2px solid #6C63FF; margin-top:9%; width:90%;">
                    <ul>
                        <form action="{{route('users.updatesecurity', $user)}}" method="post">
                        {{csrf_field() }}
                        @method('patch')
                            <li>Email</li>
                            <li><input type="text" class="form" name="email_not_attach" value="{{ $user->email_not_attach }}" autocomplete="email_not_attach"></li>
                            <li>Username</li>
                            <li><input type="text" class="form" name="email" value="{{ $user->email }}" autocomplete="email"></li>
                            <li>Password Lama</li>
                            <li><input type="password" class="form" name="password_lama" autocomplete="password_lama"></li>
                            <li>Password Baru</li>
                            <li><input type="password" class="form" name="password" autocomplete="password"></li>
                            <li>Konfirmasi Password Baru</li>
                            <li><input type="password" class="form" name="konfirmasi" autocomplete="konfirmasi"></li>
                        </ul>
                </div>
                    <input type="submit" class="Button_update" value="simpan" name="simpan">    
                    </form>
                
            </div>
        </div>
@endsection