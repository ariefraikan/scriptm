@extends('master')

@section('konten')
    <div class=container-fluid>
        <div class=row>
            <div class=col-lg-12>
                <br/>
                <br/>
                <br/>
            </div>
        </div>
        <div class=row>
            <div class=col-lg-2>
                <div class=sidebar>
                    <div id=profile_picture>
                        <img src="Profile_Picture.png"/>
                    </div>
                    <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
                    <ul>
                        
                        <li><a style="color:#FFFFFF;" href="/Profil">Profil Saya</a></li>
                        <li class=sidebar_menu><a style="color:#FFFFFF;" href="/UbahProfil">Ubah Profil</a></li>
                        <li><a style="color:#FFFFFF;" href="/editkeamanan">Ubah Keamanan</a></li>
                   
                    </ul>
					<div class=sidebar_exit>
						<h1><a href="{{ route('logout') }}">Keluar</a></h1>
                    </div>
                </div>
            </div>
            <div class=col-lg-10>
                <br/><br/><br/>
                <div class=kotak_ubahprofil>
                    <h1>Data diri</h1>
                    <hr style="border: 2px solid #6C63FF; margin-top:9%; width:90%;">
                    <ul>
                        <form action="{{route('users.update', $user)}}" method="post">
                        {{csrf_field() }}
                        @method('patch')
                            
                            <li>Nama lengkap</li>
                            <li><input type="text" class="form" name="name" value="{{ $user->name }}" id="name" autocomplete="name"></li>
                            <li>NIM</li>
                            <li><input type="text" class="form" name="NIM" value="{{ $user->NIM}}" id="NIM" autocomplete="NIM"></li>
                            <li>Jenis Kelamin</li>
                            <li><input type="text" class="form" name="jenis_kelamin" value="{{ $user->jenis_kelamin }}" id="jenis_kelamin" autocomplete="jenis_kelamin"></li>
                            <li>Nomor Handphone</li>
                            <li><input type="text" class="form" name="nomor_hp" value="{{ $user->nomor_hp }}" id="nomor_hp" autocomplete="nomor_hp"></li>
                            <li>Tempat/Tanggal Lahir</li>
                            <li><input type="text" class="form" name="TTL"value="{{ $user->TTL }}" id="TTL" autocomplete="TTL"></li>
                            
                            
                    </ul>
                </div>
                                <input type="submit" class="Button_update" value="simpan" name="simpan">    
                        </form>
                
            </div>
        </div>
    </div>
@endsection