<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title> Scriptm </title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
       
        <link rel="stylesheet" type="text/css" href="css/sty.css">
        <link rel="stylesheet" type="text/css" href="css/app.css">
    </head>

    <body>
        <div class="container-fluid px-0">
            <div class=row >
                <div class=col-lg-12 >
                    <div class=header>
                            <div id=logo>
                                <img class="float-left" src="Logo_Profil.svg" />
                            </div>
                            <h1 id=scriptm>Scriptm</h1>
                    
                    <nav class="navbar navbar-expand" position=absolute>
                    <ul class=navbar-nav position=absolute>
                        <li class=nav_item position=absolute><a  id="navbar_font"  href="/home">Beranda</a></li>
                        <li class=nav_item position=absolute><a  id="navbar_font" href="/akun">Akun</a></li>
                        <li class=nav_item position=absolute><a  id="navbar_font" href="/Galeri">Galeri</a></li>
                        <li class=nav_item position=absolute><a  id="navbar_font" href="/Kontak">Kontak</a></li>
                        </ul>
                    </nav>  
                    <div id=profile_picture>
					<a href=/Profil>
						<img src="Logo_Profile_Pictures.svg"/></a>
					</div>
                </div>
                </div>
            </div>
        </div>
       
      
        @yield('konten')
        
    </body>
</html>