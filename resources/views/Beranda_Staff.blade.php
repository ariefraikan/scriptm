<!--Panggil master-->
@extends('master2')

<!--isi konten-->
@section('konten')
    <div class=container>
        <div class=row>
           
            <div class=col-lg-12>
                <div class="kotak_info">
                    <h1>Fakultas Ilmu Komputer UPN Veteran Jakarta</h1>
                    <div id=line>
                    </div>
                    <div id=img>
                    @php $gambar = DB::table('materialize')->where('id',"1")->get();@endphp
                    @foreach($gambar as $gambar)
                    <img src="{{url('/data_file/'.$gambar->file)}}"/>
                    @endforeach
                    </div>
                    <p>scriptm adalah web Universitas UPN Veteran Jakarta yang ditujukan<br/> 
                        khusus bagi fakultas Ilmu Komputer untuk melakukan proses<br/> pengajuan
                        surat-surat yang perlu diurus ke pihak fakultas dan<br/> dilakukan secara 
                        online sehingga mempermudah proses,<br/> mempercepat waktu dan lebih 
                        efektif</p>
                    <label for="3" class="open"><img src=edit_beranda.svg style="position:left; margin-left:200%; margin-top:25%;"></label>
                    <input type="checkbox" id="3">
                    <div class=modal>
                        <div class=modal_inner>
                            <p>trying</p>
                            <label class="btn-close" for="3">X</label>
                        </div>
                    </div>
                   
                   
                </div>
            </div>
        </div>
        <div class=row>
            <div class=col-lg-12>
                <div class="kotak_berkas_keluar">
                    <a href="/Berkaskeluar">
                        <div id=kotak>
                            <div id=img>        
                                <img src=kotak_berkas_keluar_img.png />
                            </div>
                        </div>
                        <h1>Berkas Keluar</h1>
                        @foreach ($counter as $count)
                        <h2>{{$count->berkas_keluar}}</h2>
                        @endforeach
                        <div id=border>
                            <img src="kotak_berkas_border_img.svg"/>
                        </div>
                </div></a>
                <div class="kotak_berkas_masuk">
                    <a href="/Berkasmasuk">
                    <div id=kotak>
                        <div id=img>
                            <img src="kotak_berkas_masuk_img.png"/>
                        </div>
                    </div>
                    <h1>Berkas Masuk</h1>
                    @foreach($counter as $count)
                    <h2>{{$count->berkas_masuk}}</h2>
                    @endforeach
                    <div id=border>
                        <img src="kotak_berkas_border_img.svg"/>
                    </div>
                </div></a>
                <div class="kotak_progress_berkas">
                    <a href="/Progressberkas">
                    <div id=kotak>
                            <div id=img>
                                <img src="kotak_progress_berkas_img.png"/>
                            </div>
                        </div>
                        <h1>Progress Berkas</h1>
                        @foreach($counter as $count)
                        <h2>{{$count->progress_berkas}}</h2>
                        @endforeach
                        <div id=border>
                            <img src="kotak_berkas_border_img.svg"/>
                        </div> 
                    </div>
                </div></a>
                <div class="kotak_berkas_selesai">
                    <a href=/Berkasselesai>
                    <div id=kotak>
                                <div id=img>
                                    <img src="kotak_berkas_selesai_img.png"/>
                                </div>
                            </div>
                            <h1>Berkas Selesai</h1>
                            @foreach($counter as $count)
                            <h2>{{$count->berkas_selesai}}</h2>
                            @endforeach
                            <div id=border>
                                <img src="kotak_berkas_border_img.svg"/>
                            </div>
                        </div>
                </div></a>
            </div>
        </div>
    </div>  
@endsection