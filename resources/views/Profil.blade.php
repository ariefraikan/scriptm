@extends('master')

@section('konten')
    <div class=container-fluid>
        <div class=row>
            <div class=col-lg-12>
                <br/>
                <br/>
                <br/>
            </div>
        </div>
        <div class=row>
            <div class=col-lg-3>
                <div class=sidebar>
                    <div id=profile_picture>
                        <img src="Profile_Picture.png"/>
                    </div>
                    <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
                    <ul>
                        <li class=sidebar_menu><a style="color:#FFFFFF;" href="/Profil">Profil Saya</a></li>
                        <li><a style="color:#FFFFFF;" href="/edit">Ubah Profil</a></li>
                        <li><a style="color:#FFFFFF;" href="/editkeamanan">Ubah Keamanan</a></li>
                    </ul>
					<div class=sidebar_exit>
						<h1><a href="{{ route('logout') }}">Keluar</a></h1>
                    </div>
                </div>
            </div>
            <div class=col-lg-9 >
                <br/><br/><br/>
                <div class=kotak_datadiri>
                    <ul>
                        
                        
                            <li>Nama Lengkap</li>
                            <li class=form>{{Auth::user()->name}}</li>
                            <li>NIM</li>
                            <li class=form>{{Auth::user()->NIM}}</li>
                            <li>Jenis Kelamin</li>
                            <li class=form>{{auth::user()->jenis_kelamin}}</li>
                            <li>Nomor Handphone</li>
                            <li class=form>{{auth::user()->nomor_hp}}</li>
                            <li>Tempat/Tanggal Lahir</li>
                            <li class=form>{{auth::user()->TTL}}</li>
                            <li>Email</li>
                            <li class=form>{{auth::user()->email_not_attach}}</li>
                        
                        
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection('konten')