<!--Panggil master-->
@extends('master')

<!--isi konten-->
@section('konten')
    <div class=container>
        <div class=row>
            <div class=col-lg-12>
                <div class=kotak_berkaskeluar>
                    <h1>Berkas Keluar</h1>
                </div>
               
                <div class=card style="margin-top:20%">
                    <div class=table>
                        <table width="1200px">
                            <thead>
                                <tr>
                                    <th>Nama</th>
                                    <th>Edit</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($fileupload as $file)
                                <tr>
                                    <td>
                                    <a href="{{url('/data_file/'.$file->file)}}">
                                    {{$file->file}}</a>
                                    </td>
                                    <td>
                                    <a href="{{url('/data_file/'.$file->file)}}" download="{{$file->file}}"><img src="bx_bxs-download.png" >
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                    Halaman :{{$fileupload->currentPage()}}<br/>
                    Jumlah data :{{$fileupload->total()}}<br/>
                    Data per Halaman :{{$fileupload->perPage()}}<br/>

                    {{$fileupload->links()}}
            
            </div>
        </div>
    </div>




@endsection