<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title> Scriptm </title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
       
        <link rel="stylesheet" type="text/css" href="css/sty.css">
        <link rel="stylesheet" type="text/css" href="css/app.css">
    </head>

    <body>
        
                    <div class=header>
                            <div id=logo>
                                <img class="float-left" src="Logo_Profil.svg" />
                            </div>
                            <h1 id=scriptm>Scriptm</h1>
                    
                    <nav class="navbar navbar-expand" position=absolute>
                    <ul class=navbar-nav position=absolute>
                        <li class=nav_item position=absolute><a  id="navbar_font"  href="/Beranda">Beranda</a></li>
                        <li    class=nav_item position=absolute><a  id="navbar_font">
                           
                            <div class=dropdown >
                            <button class="kategori" >Kategori</button>
						    <div class="dropdown-content">
						    <a href="">Pengajuan Proposal</a>
						    <a href="/PengajuanSKMA">Pengajuan Surat Keterangan Mahasiswa Aktif</a>
						    <a href="">Pengajuan Sertifikat</a>
						    <a href="">Pengajuan Surat Tidak Menerima Beasiswa</a>
						    <a href="">Pengajuan Surat Magang</a>
						    <a href="">Pengajuan Stiker Parkir Mobil</a>
                            </div>
                            </div>
                            </a></li>
                        <li class=nav_item position=absolute><a  id="navbar_font" href="/Galeri">Galeri</a></li>
                        <li class=nav_item position=absolute><a  id="navbar_font" href="/Kontak">Kontak</a></li>
                        </ul>
                    </nav>  
                    <div id=profile_picture>
					<a href=/Profil>
						<img src="Logo_Profile_Pictures.svg"/></a>
					</div>
                </div>
                </div>
            </div>
        </div>
       
      
        @yield('konten')
        
    </body>
</html>