<!--extends from master-->
@extends('master')

<!--isi konten-->
@section('konten')
    <div class=container>
        <div class=row>
            <div class=col-lg-12>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
            </div>
        </div>
        <div class=row>
            <div class=col-lg-12>
            <form action="" method="GET">
                <input type="text" class="cari_teks" placeholder="Cari di Galeri" value="{{old('cari')}}">
		        <input type="image" src="galeri_cari_button.svg" class="cari_button" value="cari">
            </form>
            <br/>
            <br/>
            </div>
        </div>
        <div class=row>
            <div class=card>
                <div class=table>
                    <table width="1200px">
                        <thead>
                            <tr>
                                <th>Nama</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                           
                            @foreach ($fileupload as $file)
                            <tr>
                                <td>
                                <a href="{{url('/data_file/'.$file->file)}}">
                                {{$file->file}}</a>
                                </td>
                                <td>
                                <a href="{{url('/data_file/'.$file->file)}}" download="{{$file->file}}"><img src="bx_bxs-download.png" >
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <br/>
                </div>
            </div>
                Halaman :{{$fileupload->currentPage()}}<br/>
                Jumlah data :{{$fileupload->total()}}<br/>
                Data per Halaman :{{$fileupload->perPage()}}<br/>

                {{$fileupload->links()}}
        </div>
    </div>
   
    

    
            
@endsection