@extends('layout')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header" style="color:white; background-color:#6C63FF; font-family: Roboto;
                font-style: normal; font-weight: bold;">{{ __('Tambah Kontak') }}
                <a href="" style="margin-left:95%;"><img src="Vector.svg" ></a>
                </div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}" >
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-left" style="font-family: Roboto;
                            font-style: normal; font-weight: bold;">{{ __('Nama') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-left" style="font-family: Roboto;
                            font-style: normal; font-weight: bold;">{{ __('Username') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-left" style="font-family: Roboto;
                            font-style: normal; font-weight: bold;">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-left" style="font-family: Roboto;
                            font-style: normal; font-weight: bold;">{{ __('Konfirmasi Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="role_akun" class="col-md-4 col-form-label text-md-left radio-inline" style="font-family: Roboto;
                            font-style: normal; font-weight: bold;">{{ __('Jenis Akun') }}</label>

                            <div class="col-md-6">
                                <input id="role_akun" type="radio" class="form-control @error('role_akun') is-invalid @enderror" name="role_akun" value="Mahasiswa" style="font-family: Roboto;
                                font-style: normal; font-weight: bold;">Mahasiswa</input>
                                <input id="role_akun" type="radio" class="form-control @error('role_akun') is-invalid @enderror" name="role_akun" value="Staff" style="font-family: Roboto;
                                font-style: normal; font-weight: bold;">Staff</input>
                                @error('role_akun')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            </div>
                        </div>
                        <input type=hidden  name="NIM" id="NIM">
                        <input type=hidden  name="jenis_kelamin" id="jenis_kelamin">
                        <input type=hidden  name="nomor_hp" id="nomor_hp">
                        <input type=hidden  name="TTL" id="TTL">
                        <input type=hidden  name="email_not_attach" id="email_not_attach">
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary" style="background-color:#6C63FF; color:white;">
                                    {{ __('Simpan') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection