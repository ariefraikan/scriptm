@extends('master')

@section('konten')
    <div class=container>
        <div class=row>
            <div class=col-lg-12>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
            </div>
        </div>
        <div class=row>
            <div class=col-lg-12>
                <div class=kotakjudul>
                    <h1>Pengajuan Surat Keterangan Mahasiswa Aktif</h1>
					<div id=img>
						<img src="Group 22.png"/>
					</div>
					<div id=img2>
						<img src="Group 23.png"/>
					</div>
                </div>
                <br/>
                <div class=kotakdataprofil>
                    <h1>Data yang dibutuhkan sesuai profil</h1>
                    <div id=line>
                    </div>
                    <ul>
                        <li>Nama Lengkap:{{Auth::user()->name}}</li>
                        <li>NIM                                             :{{Auth::user()->NIM}}</li>
                        <li>Jenis Kelamin                       :{{Auth::user()->jenis_kelamin}}</li>
                        <li>Tempat/Tanggal Lahir                :{{Auth::user()->TTL}}</li>
                        <li>Nomor Handphone                     :{{Auth::user()->nomor_hp}}</li>
                        <li>Email                               :{{Auth::user()->email_not_attach}}</li>
                    </ul>
                </div>   
                <div class=kotakdataupload>
                    <h1>Data yang harus dilengkapi</h1>
                    <div id=line>
                    </div>
                    <ul>
                        <form method="post" enctype="multipart/form-data" action="/upload_SKMA">
                        {{csrf_field()}}
                        <li>KTM                                                                                               <input type=file name=file ></li>
                        <li>Foto Bewarna 3x4                                                                         <input type=file name=file2 ></li>
                        <input type=hidden value="{{ Auth::user()->id}}" name="user_id">
                        <input type=hidden  name="pesan">
                        <li><input type=submit value="upload"  ></li> 
                        </form>
                    </ul>
                </div> 
            </div>
        </div>
    </div>
@endsection