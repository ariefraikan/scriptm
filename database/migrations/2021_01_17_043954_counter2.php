<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Counter2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('counter', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->integer('berkas_masuk');
            $table->integer('berkas_keluar');
            $table->integer('progress_berkas');
            $table->integer('berkas_selesai');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('counter');
    }
}
