<!DOCTYPE html> 
<html lang="en"> 
<head>
<meta charset="utf-8">
<title>Login</title>
	<link rel="stylesheet" type="text/css" href="css/sty.css">
</head> 

<body> 
<div class=wrap>
	<div class=left>
		<h1>Pengajuan Surat Lebih Mudah!</h1>
		<p>Scriptm adalah web Universitas UPN Veteran Jakarta yang ditujukan khusus bagi fakultas ilmu komputer untuk melakukan proses pengajuan surat-surat yang perlu
		diurus ke pihak fakultas dan dilakukan secara online sehingga mempermudah proses, mempercapat waktu dan lebih efektif.</p>
		<div id=img1 class="left">
		<img src="images/login.png" />
		</div>
		<div id=img2 class="left">
		<img src="images/Group 14.png" />
		</div>
	</div>
	<div class=right>
		<img src="images/Group 12.png" />
		<div class="kotak_login">
		<form method='post' action="login_check.php">
			<input type="text" name="Username"  placeholder="Username" class="form_login">
			<input type="password" name="Password"  placeholder="Password" class="form_login">
			<input type="submit"value="Login" class="tombol_login">
		</form>
		<?php
			if(isset($_GET['pesan'])){
				if($_GET['pesan'] == "failed"){
					echo '<span style="color:red; text-align:center;>Login gagal! Silahkan masukkan username atau password yang tepat</span>';
				}
				
			}
		?>
		
		</div>
	</div>
	
</body> 
</html> 